<?php

namespace Blue\Api\Controllers;

use App\Http\Controllers\Controller;
use Blue\Api\Models\Item;
use Blue\Api\Requests\ItemData;
use Blue\Api\Requests\Search;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function list(Search $request)
    {
        $builder = Item::query();

        // Move search code to a service when adding new controllers
        if ($request->has('limit')) {
            $builder->simplePaginate($request->limit, null, 'page', $request->page ?? 1);
        }

        if ($request->has('query')) {
            $query = $request->get('query');
            $object = new Item;
            $columns = \Illuminate\Support\Facades\Schema::getColumnListing($object->getTable());
            foreach ($query as $key => $params) {
                if (in_array($key, $columns)) {
                    if ($params['operator'] === 'IN') {
                        $builder->whereIn($key, (array) $params['value']);
                    } elseif ($params['operator'] === 'LIKE') {
                        $builder->where($key, 'LIKE', "%$params[value]%");
                    } else {
                        $builder->where($key, $params['operator'], $params['value']);
                    }
                }
            }
        }

        $result = $builder->get();

        // Bugfix? Laravel returns limit+1 results X_x
        if ($request->limit) {
            $result = $result->chunk($request->limit)[0] ?? [];
        }
        return $result;
    }

    public function show(Item $item)
    {
        return $item;
    }

    public function create(ItemData $request)
    {
        return $this->update(new Item, $request);
    }

    public function update(Item $item, ItemData $request)
    {
        $item->name = $request->name;
        $item->amount = $request->amount;
        $item->save();

        return $item;
    }

    public function delete(Item $item)
    {
        return ['success' => $item->delete()];
    }
}
