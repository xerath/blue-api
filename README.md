# Blue Services API
This is a laravel package providing api solution to the homework.

## Install
Add the repo to your `composer.json` file

```
{
    "repositories": [
        {
            "url": "https://bitbucket.org/xerath/blue-api.git",
            "type": "git"
        }
    ],
}
```

Run the command

```
composer require blue/api
```

## Configuration
In the `.env` file you need to configure the dingo/api package: https://github.com/dingo/api/wiki/Configuration
