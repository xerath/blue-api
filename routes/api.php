<?php

use Dingo\Api\Routing\Router as DingoRouter;
use Blue\Api\Controllers as Controllers;

/* @var $api DingoRouter */
$api = app(DingoRouter::class);

app(Dingo\Api\Exception\Handler::class)->register(function(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
    throw new Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
});

// Fix for Illuminate\Validation\ValidationException not being a HTTP exception
app(Dingo\Api\Exception\Handler::class)->register(function(Illuminate\Validation\ValidationException $e) {
    $response = (new Dingo\Api\Exception\Handler(resolve('Illuminate\Contracts\Debug\ExceptionHandler'), config('api.errorFormat'), config('api.debug')))->handle($e)->setStatusCode(422);
    return $response;
});

$api->version('v1', ['middleware' => ['api', 'bindings']], function(DingoRouter $api) {
    $api->get('item', Controllers\ItemController::class . '@list');
    $api->get('item/{item}', Controllers\ItemController::class . '@show');
    $api->post('item', Controllers\ItemController::class . '@create');
    $api->post('item/{item}', Controllers\ItemController::class . '@update');
    $api->delete('item/{item}', Controllers\ItemController::class . '@delete');
});
